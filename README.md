Memory 2000
===

## Installation

* le projet repose sur Meteor, il faut donc installer Meteor avant toutes choses : ```curl https://install.meteor.com/ | sh```
* cloner le projet
* aller dans le dossier
* initialiser les modules node : ```meteor npm i```
* le reste sera compilé au lancement

## Lancement 

* ```meteor``` dans le dossier du projet

## Déploiement

* installer ```npm install --global mup```
* remplir le fichier *.deploy/mup.js* avec les informations pertinentes à votre déploiement
* ```cd .deploy```
* ```mup setup``` pour configurer le serveur
* ```mup deploy``` pour déployer le code

## Description du code

* la partie cliente se trouve principalement dans *imports/ui/App.jsx*. Il s'agit d'une application React qui utilise le state pour faire varier l'affichage. Il y a 3 écrans possible : l'accueil, le jeu et un écran de fin affichant le score.
* on utilise scss pour le preprocessing css, tout le code se trouve dans *client/main.scss*. On utilise une boucle afin dé générer toutes les classes correspondant aux différents fruits.
* la partie serveur se trouve simplement dans *server/main.js* où sont déclarés à la fois la base de données et les fonctions de l'API pour le front. Il y a une unique collection *Score* qui permet de garder en mémoire les scores des parties terminées.