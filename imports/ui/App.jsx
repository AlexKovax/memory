import React from 'react';
import { Meteor } from 'meteor/meteor';

class App extends React.Component {

  constructor() {
    super();
    this.state = {
      step: 'welcome',
      tabMemory: [],//ce tableau contiendra la liste des cartes du jeu
      tabUserFound: [],//ce tableau contiendra la liste des positions des cartes trouvées par l'utilisateur
      firstChoice: null,//cette variable contiendra la position de la première carte choisie par l'utilisateur
      secondChoice: null,//cette variable contiendra la position de la première carte choisie par l'utilisateur
      tabTimes: [],//ce tableau contiendra les meilleurs scores pour afficher sur la première page
      time: 0,//compteur de temps
      userName: ''
    }
    this.timer = null;
    this.maxTime = 5 * 60;//On met le maximum à 5 minutes
  }

  componentDidMount() {
    //Au chargement on va chercher tout les scores dans la base en appelant l'API et on les mets dans le state
    Meteor.call('getScores', (err, res) => {
      if (err) {
        console.log(err)
      } else {
        this.setState({ tabTimes: res })
      }
    })

  }

  handleClick(position) {

    if (this.state.firstChoice === position) {
      //si le joueur reclique sur la même carte rien ne se passe
      return;
    }

    //On enregistre les choix de l'utilisateur dans le state
    if (this.state.firstChoice === null) {
      //Si le premier choix n'est pas encore enregistré, on le positionne et on sort de la fonction
      return this.setState({ firstChoice: position });
    }

    if (this.state.secondChoice === null) {
      //Si le second choix, n'est pas fais on l'enregistre (pour l'affichage)
      this.setState({ secondChoice: position });

      //Puis on regarde si l'utilisateur a choisi deux fois la même carte ou non
      if (this.state.tabMemory[this.state.firstChoice] === this.state.tabMemory[position]) {
        //Cas 1 : paires trouvée
        let tab = this.state.tabUserFound;
        tab.push(this.state.firstChoice);
        tab.push(position);
        this.setState({ tabUserFound: tab, firstChoice: null, secondChoice: null })

        //Il faut également tester si c'est la dernière paire à trouver
        //Pour cela on verifie simplement que le tableau des choix de l'utilisateur fait la même longueur que le tableau de jeu
        if (tab.length === this.state.tabMemory.length) {
          let name = prompt('Bravo, vous avez gagné ! Quel est votre nom pour notre leaderboard ?')
          if (name !== '') {
            Meteor.call('addScore', name, this.state.time);//On envoie le score à la base de données
            clearInterval(this.timer);//on stoppe le chronometre
            this.setState({ step: 'end', userName: name });//on passe à l'affichage de victoire
          }
        }
      } else {
        //Cas 2 : paire non trouvées
        //Dans ce cas, on retourne les cartes au bout d'une demi seconde et le tour est fini
        setTimeout(() => {
          this.setState({ firstChoice: null, secondChoice: null })
        }, 500)

      }
    }

  }

  pressStart() {
    //Initialisation du jeu
    //Pour initialiser on crée un tableau qui contiendra la liste des fruits
    let tabJeu = [];
    for (let i = 0; i < 14; i++) {
      let fruit = 'fruit-' + i;
      tabJeu.push(fruit);
      tabJeu.push(fruit);
    }

    //On mélange le tableau
    tabJeu = tabJeu.sort(function () { return 0.5 - Math.random() })

    this.setState({ step: 'game', tabMemory: tabJeu });

    //Lancement chrono
    this.timer = setInterval(() => {
      //on vérifie à chaque fois si le compteur n'a pas atteint le temps limite
      if (this.state.time < this.maxTime) {
        this.setState({ time: ++this.state.time })
      } else {
        //le temps limite est atteint, on utilise le state pour afficher le game over
        this.setState({ step: 'gameover' })
        clearInterval(this.timer);//on stoppe le chronometre
      }

    }, 1000)
  }

  render() {

    if (this.state.step === 'welcome') {
      //ECRAN ACCUEIL
      return (
        <div>
          <h1>Welcome to Memory 2000</h1>
          <h2>High scores</h2>
          <ul>
            {this.state.tabTimes.map((score, i) => {
              //affichage de chaque score récupéré dans le state
              return (
                <li key={i}>{i + 1}. {score.name} - {score.time}s</li>
              )
            })}
          </ul>
          <div style={{ textAlign: 'center' }}>
            <button onClick={this.pressStart.bind(this)}>New game !</button>
          </div >
        </div >
      )
    } else if (this.state.step === 'game') {
      //JEU
      return (
        <div>
          <h1>Memory 2000</h1>
          <h2>Time : {this.state.time}s</h2>

          <div id='progressBar'>
            <div id='progressIndicator' style={{ width: parseInt(Math.ceil(this.state.time * 100 / this.maxTime)) + '%' }} />
          </div>

          <div className='container'>
            {this.state.tabMemory.map((item, position) => {
              //Pour chaque élément du tableau, on crée une div qui correspondra à un fruit/carte
              let classes = item;

              //On doit vérifier pour chaque carte si elle doit être affichée ou non. Elle doit être affichée si :
              //elle est dans le tableau des cartes trouvées par l'utilisateur
              //OU elle est la première carte cliquée par l'utilisateur à ce tour
              //OU elle est la deuxième carte cliquée par l'utilisateur à ce tour
              if (this.state.tabUserFound.indexOf(position) === -1 && this.state.firstChoice !== position && this.state.secondChoice !== position) {
                classes += ' hidden';
              }

              //On envoi le jsx de la carte avec une action onclick pour gérer les actions de jeu
              return (
                <div className={classes} key={position} onClick={this.handleClick.bind(this, position)}></div>
              )
            })}
          </div>
        </div>
      )
    } else if (this.state.step === 'end') {
      //ECRAN FIN
      return (
        <div>
          <h1>Memory 2000</h1>
          <h2>Félicitations {this.state.userName}, vous avez mis {this.state.time}s ! Bravo</h2>
        </div >
      )
    } else if (this.state.step === 'gameover') {
      //ECRAN GAME OVER
      return (
        <div>
          <h1>Memory 2000</h1>
          <h2>Désolé le temps est écoulé, vous avez perdu !</h2>
        </div >
      )
    }

  }

}

export default App;
