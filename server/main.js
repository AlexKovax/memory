import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

//Déclaration de la table permettant de collecter les scores
Scores = new Mongo.Collection('scores');

//Déclaration de l'API pour travailler avec la base
Meteor.methods({
  //getScores permet de récupérer les 10 derniers scores dans la base de données, classés du plus petit au plus grand
  getScores() {
    return Scores.find({}, { sort: { time: 1 }, limit: 10 }).fetch();
  },
  //addScore permet d'insérer un score dans la base de données
  addScore(name, time) {
    Scores.insert({ name, time, createdAt: new Date() })
  }
})
